from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoe


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["color"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["color"]


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all.order_by("bin_id")
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe_detail(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400
            )
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Shoe does not exist"},
                status=400,
            )
        shoe.delete()
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        shoe = Shoe.objects.filter(id=pk).update(**content)
        return JsonResponse(
            shoe,
            ShoeDetailEncoder,
            safe=False,
        )
