from django.db import models


class Shoe (models.Model):
    color = models.CharField(max_length=200, blank=True)
