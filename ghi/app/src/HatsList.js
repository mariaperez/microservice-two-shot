import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';

function Hat(props) {
    const { hat } = props;
    return (
        <div key={hat.id} className="card mb-3 shadow">
            <NavLink to={`/hats/${hat.id}`} className="card-link">
                <img src={hat.picture} className="card-img-top" alt="" />
                <div className="card-body">
                    <h5 className="card-title">{hat.color} {hat.fabric} {hat.style_name}</h5>
                </div>
            </NavLink>
            <div className="card-footer">
                <div className="btn" onClick={async () => await fetch(`http://localhost:8090/api/hats/${hat.id}/`, { method: "delete" })}>
                    Remove Hat
                </div>
            </div>
        </div>
    );
}


function HatsList() {
    const [hats, setHats] = useState([]);
    const [locations, setLocations] = useState([]);

    const fetchHatData = async () => {
        const url = 'http://localhost:8090/api/hats/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    };

    const fetchLocationData = async () => {
        const url = 'http://localhost:8100/api/locations';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    useEffect(() => {
        fetchHatData();
        fetchLocationData();
    }, []);

    return (
        <div className="container">
            <h1 className="text-center mt-5">All of your hats</h1>
            <div className="text-center mb-3">
                <NavLink className="btn btn-primary" to="/hats/new">Add hat</NavLink>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <h2 className="text-center mb-3">Hats List:</h2>
                    {hats.map(hat => (
                        <Hat hat={hat} key={hat.id} />
                    ))}
                </div>
            </div>
        </div>
    );
}

export default HatsList;
