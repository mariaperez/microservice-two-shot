import React, { useEffect, useState } from 'react';

function HatDetail(props) {
    const { match } = props;
    const [hat, setHat] = useState(null);

    useEffect(() => {
        const fetchHatDetail = async () => {
            const hatId = match.params.id;
            const response = await fetch(`http://localhost:8090/api/hats/${hatId}/`);
            if (response.ok) {
                const data = await response.json();
                setHat(data);
            }
        };

        fetchHatDetail();
    }, [match.params.id]);

    if (!hat) {
        return <div>Loading...</div>;
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col-md-6">
                    <img src={hat.picture} className="img-fluid" alt="" />
                </div>
                <div className="col-md-6">
                    <h2>{hat.style_name}</h2>
                    <p>
                        <strong>Color:</strong> {hat.color}
                    </p>
                    <p>
                        <strong>Fabric:</strong> {hat.fabric}
                    </p>
                    <p>
                        <strong>Description:</strong> {hat.description}
                    </p>
                </div>
            </div>
        </div>
    );
}

export default HatDetail;
