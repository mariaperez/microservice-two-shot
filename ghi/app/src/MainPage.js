function MainPage() {
  return (
      <div className="container">
          <div className="px-4 py-5 my-5 text-center">
              <h1 className="display-4 fw-bold">WARDROBIFY!</h1>
              <div className="col-lg-8 mx-auto">
                  <p className="lead mb-4">
                      Need to keep track of your shoes and hats? We have the solution for you! Wardrobify allows you to organize and manage your hat collection with ease. From style names to colors and fabrics, everything is just a click away.
                  </p>
                  <p className="lead mb-4">
                      Never forget where you placed your favorite hats again. With Wardrobify, you can categorize your hats by location, making it effortless to find the perfect hat for any occasion. Explore your collection, add new hats, and remove the ones you no longer need, all in one place.
                  </p>
                  <p className="lead mb-4">
                      Start organizing your hat collection today with Wardrobify!
                  </p>
                  <button className="btn btn-primary btn-lg">Get Started</button>
              </div>
          </div>
      </div>
  );
}

export default MainPage;
